import React from 'react';
import  ReactDOM  from 'react-dom';
import './style.css';




   
function Zodiac () {
  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>Символ</th>
            <th>Знак Зодіаку</th>
            <th>Дата Народження</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>&#9800;</td>
            <td>Овен</td>
            <td>21 березня – 20 квітня</td>
          </tr>
          <tr>
            <td>&#9801;</td>
            <td>Телець</td>
            <td>21 квітня – 21 травня</td>
          </tr>
          <tr>
            <td>&#9802;</td>
            <td>Близнюки</td>
            <td>22 травня – 21 червня</td>
          </tr>
          <tr>
            <td>&#9803;</td>
            <td>Рак</td>
            <td>22 червня – 22 липня</td>
          </tr>
          <tr>
            <td>&#9804;</td>
            <td>Лев</td>
            <td>23 липня – 21 серпня</td>
          </tr>
          <tr>
            <td>&#9805;</td>
            <td>Діва</td>
            <td>22 серпня – 23 вересня</td>
          </tr>
          <tr>
            <td>&#9806;</td>
            <td>Терези</td>
            <td>24 вересня – 23 жовтня</td>
          </tr>
          <tr>
            <td>&#9807;</td>
            <td>Скорпіон</td>
            <td>24 жовтня – 23 листопада</td>
          </tr>
          <tr>
            <td>&#9808;</td>
            <td>Стрілець</td>
            <td>24 листопада – 22 грудня</td>
          </tr>
          <tr>
            <td>&#9809;</td>
            <td>Козоріг</td>
            <td>23 грудня – 20 січня</td>
          </tr>
          <tr>
            <td>&#9810;</td>
            <td>Водолій</td>
            <td>21 січня – 19 лютого</td>
          </tr>
          <tr>
            <td>&#9811;</td>
            <td>Риби</td>
            <td>20 лютого – 20 березня</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
  
ReactDOM.render(<Zodiac></Zodiac>,document.querySelector("#root"))
